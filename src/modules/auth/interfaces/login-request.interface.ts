import { User } from '../../user/entities/user.entity';

export interface ILoginRequest {
  username: string;
  password: string;
  userId?: string;
  user?: User;
}
