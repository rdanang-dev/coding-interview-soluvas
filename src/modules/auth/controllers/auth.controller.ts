import { Body, Controller, Post, Req, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiOperation, ApiTags } from '@nestjs/swagger';

import { BaseResponse } from '../../../core/dto/base-response';
import { User } from '../../user/entities/user.entity';
import { UserService } from '../../user/services/user.service';
import { RegisterRequest } from '../dtos/requests/register.request';
import { LoginResponse } from '../dtos/response/login-response';
import { ILoginRequest } from '../interfaces/login-request.interface';
import { AuthService } from '../services/auth.service';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(
    private authService: AuthService,
    private userService: UserService,
  ) {}

  /**
   * Login Endpoint
   * @param req LoginRequest
   * @returns {Promise<BaseResponse<LoginResponse>>} LoginResponse
   */
  @ApiOperation({ summary: 'Login' })
  @UseGuards(AuthGuard('local'))
  @Post('login')
  async login(@Req() req: ILoginRequest): Promise<BaseResponse<LoginResponse>> {
    const submitLogin = await this.authService.login(req.user);
    const response = new BaseResponse<LoginResponse>();
    response.data = submitLogin;
    return response;
  }

  /**
   * Register Endpoint
   * @param registerRequest RegisterRequest
   * @returns {Promise<BaseResponse<User>>} Registered User Data.
   */
  @ApiOperation({ summary: 'Register' })
  @Post('register')
  async register(
    @Body() registerRequest: RegisterRequest,
  ): Promise<BaseResponse<User>> {
    const createdUser = await this.authService.register(registerRequest);
    const response = new BaseResponse<User>();
    response.data = createdUser;
    return response;
  }
}
