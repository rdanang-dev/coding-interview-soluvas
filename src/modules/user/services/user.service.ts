import { BadRequestException, Injectable, Logger, NotFoundException } from '@nestjs/common';
import { FilterQuery } from 'mongoose';

import { formatPhoneNumber } from '../../../core/utils/formatPhoneNumber';
import { hashPassword } from '../../../core/utils/passwordHash';
import { UserCreateRequest } from '../dtos/requests/user-create.request';
import { UserFilterRequest } from '../dtos/requests/user-filter.request';
import { UserUpdateRequest } from '../dtos/requests/user-update.request';
import { User, UserDocument } from '../entities/user.entity';
import { UserRepository } from '../repositories/user.repository';

@Injectable()
export class UserService {
  constructor(private userRepository: UserRepository) {}

  private readonly logger = new Logger(UserService.name);

  async findAllUserPaginated(
    filterRequest: UserFilterRequest,
  ): Promise<User[]> {
    return await this.userRepository.findAllUserPaginated(filterRequest);
  }

  async countUser(filterRequest: UserFilterRequest): Promise<number> {
    return await this.userRepository.countUser(filterRequest);
  }

  async createUser(
    creatorId: string,
    createRequest: UserCreateRequest,
  ): Promise<User> {
    let filter: FilterQuery<UserDocument> = {};

    filter = {
      $or: [
        { email: createRequest.email },
        { username: createRequest.username },
      ],
    };

    // find if user is already exists
    const checkExists = await this.userRepository.findOne(filter);

    // throw bad request if user already exists
    if (checkExists) {
      throw new BadRequestException('Username / Email already exitst');
    }
    const userData = User.fromCreateDto(createRequest);
    if (userData.password) {
      userData.password = await hashPassword(userData.password);
    }
    if (createRequest.phoneNumber) {
      userData.phoneNumber = formatPhoneNumber(
        createRequest.phoneNumber,
      ).substring(1);
    }
    return await this.userRepository.create(userData, creatorId);
  }

  async findUserById(id: string): Promise<User> {
    const findUserById = await this.userRepository.findById(id);

    if (!findUserById) {
      throw new NotFoundException(`User with id ${id} not found`);
    }

    return findUserById;
  }

  async updateUserById(
    id: string,
    updateRequest: UserUpdateRequest,
  ): Promise<User> {
    await this.findUserById(id);

    const updateUserData = User.fromUpdateDto(updateRequest);
    if (updateUserData.password) {
      updateUserData.password = await hashPassword(updateUserData.password);
    }
    if (updateRequest.phoneNumber) {
      updateUserData.phoneNumber = formatPhoneNumber(
        updateRequest.phoneNumber,
      ).substring(1);
    }
    return await this.userRepository.updateById(id, updateUserData);
  }

  async updateCurrentUser(
    userId: string,
    updateRequest: UserUpdateRequest,
  ): Promise<User> {
    const updateUserData = User.fromUpdateDto(updateRequest);
    return await this.userRepository.updateById(userId, updateUserData);
  }

  async deleteUserById(id: string): Promise<User> {
    return this.userRepository.deleteById(id);
  }
}
