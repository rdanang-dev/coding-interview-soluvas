import { Body, Controller, Delete, Get, Param, Post, Put, Res, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { Response } from 'express';

import { CurrentUser } from '../../../core/decorators/current-user.decorator';
import { Roles } from '../../../core/decorators/roles.decorator';
import { BaseResponse } from '../../../core/dto/base-response';
import { PaginatedResponse } from '../../../core/dto/paginated-response.dto';
import { baseResponseHelper } from '../../../core/helpers/base-response-helper';
import { paginationHelper } from '../../../core/helpers/pagination-helper';
import { validateObjectId } from '../../../core/utils/validateObjectId';
import { JwtAuthGuard } from '../../auth/guards/jwt-auth.guard';
import { RolesGuard } from '../../auth/guards/roles.guard';
import { UserCreateRequest } from '../dtos/requests/user-create.request';
import { UserFilterRequest } from '../dtos/requests/user-filter.request';
import { UserUpdateRequest } from '../dtos/requests/user-update.request';
import { User } from '../entities/user.entity';
import { RoleEnum } from '../enums/role.enum';
import { ICurrentUser } from '../interfaces/current-user.interface';
import { UserService } from '../services/user.service';

@ApiTags('users')
@Controller('users')
export class UserController {
  constructor(private userService: UserService) {}

  @ApiOperation({
    summary: 'Find All User With Pagination.',
  })
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth('JWT-Auth')
  @Get('/users')
  async getAllUserPaginated(
    @Body() filterRequest: UserFilterRequest,
    @Res() res: Response,
  ): Promise<Response<PaginatedResponse<User[]>>> {
    const { limit, page } = filterRequest;
    const paginatedBot = await this.userService.findAllUserPaginated(
      filterRequest,
    );
    const botCount = await this.userService.countUser(filterRequest);
    const response = paginationHelper<User[]>(
      res,
      paginatedBot,
      botCount,
      page,
      limit,
    );
    return response;
  }

  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(RoleEnum.SUPER_USER, RoleEnum.ADMIN)
  @ApiBearerAuth('JWT-Auth')
  @Post('/')
  async createUser(
    @CurrentUser() user: ICurrentUser,
    @Body() createRequest: UserCreateRequest,
    @Res() res: Response,
  ): Promise<Response<BaseResponse<User>>> {
    const createdUser = await this.userService.createUser(
      user.userId,
      createRequest,
    );
    return baseResponseHelper(res, createdUser);
  }

  @Get('/current-user')
  @UseGuards(JwtAuthGuard)
  async getCurrentAuthenticatedUser(
    @CurrentUser() user: ICurrentUser,
    @Res() res: Response,
  ): Promise<Response<BaseResponse<User>>> {
    const getCurrentUser = await this.userService.findUserById(user.userId);
    return baseResponseHelper(res, getCurrentUser);
  }

  @Get('/:id')
  async findUserById(
    @Param('id') id: string,
    @Res() res: Response,
  ): Promise<Response<BaseResponse<User>>> {
    await validateObjectId(id);
    const user = await this.userService.findUserById(id);
    return baseResponseHelper(res, user);
  }

  @Put('/current-user/update')
  @UseGuards(JwtAuthGuard)
  async updateCurrentUser(
    @CurrentUser() user: ICurrentUser,
    @Body() updateRequest: UserUpdateRequest,
    @Res() res: Response,
  ): Promise<Response<BaseResponse<User>>> {
    const updatedCurrentUser = await this.userService.updateCurrentUser(
      user.userId,
      updateRequest,
    );
    return baseResponseHelper(res, updatedCurrentUser);
  }

  @Put('/:id')
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(RoleEnum.SUPER_USER, RoleEnum.ADMIN)
  async updateUser(
    @Param('id') id: string,
    @Body() updateRequest: UserUpdateRequest,
    @Res() res: Response,
  ): Promise<Response<BaseResponse<User>>> {
    await validateObjectId(id);
    const updatedUser = await this.userService.updateUserById(
      id,
      updateRequest,
    );
    return baseResponseHelper(res, updatedUser);
  }

  @Delete('/:id')
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(RoleEnum.SUPER_USER, RoleEnum.ADMIN)
  async deleteUser(
    @Param('id') id: string,
    @Res() res: Response,
  ): Promise<Response<BaseResponse<User>>> {
    await validateObjectId(id);
    const deletedUser = await this.userService.deleteUserById(id);
    return baseResponseHelper(res, deletedUser);
  }
}
