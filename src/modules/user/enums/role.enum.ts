export enum RoleEnum {
  SUPER_USER = 'super_user',
  ADMIN = 'admin',
  TEACHER = 'teacher',
  STUDENT = 'student',
}
