import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { FilterQuery, Model, Types } from 'mongoose';

import { UserFilterRequest } from '../dtos/requests/user-filter.request';
import { User, UserDocument } from '../entities/user.entity';

@Injectable()
export class UserRepository {
  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) {}

  async create(user: User, creatorId: string | null): Promise<User> {
    user.creatorId = creatorId ? new Types.ObjectId(creatorId) : null;
    return new this.userModel(user).save();
  }

  async filterUser(
    filter: UserFilterRequest,
  ): Promise<FilterQuery<UserDocument>> {
    const filterQuery: FilterQuery<UserDocument> = {};
    const { username, email, fullName, role, phoneNumber, creatorId } = filter;
    if (username) {
      filterQuery.username = { $regex: username, $options: 'i' };
    }
    if (email) {
      filterQuery.email = { $regex: email, $options: 'i' };
    }
    if (fullName) {
      filterQuery.fullName = { $regex: fullName, $options: 'i' };
    }
    if (phoneNumber) {
      filterQuery.phoneNumber = { $regex: phoneNumber, $options: 'i' };
    }
    if (role) {
      filterQuery.role = role;
    }
    if (creatorId) {
      filterQuery.creatorId = new Types.ObjectId(creatorId);
    }
    return filterQuery;
  }

  async findAllUserPaginated(
    request: UserFilterRequest,
  ): Promise<UserDocument[]> {
    const { page, limit } = request;
    const filterQuery = await this.filterUser(request);
    const offset = (page - 1) * limit;
    return await this.userModel
      .find(filterQuery)
      .skip(offset)
      .limit(limit)
      .populate('createdBy')
      .exec();
  }

  async findPaginate() {
    return this.userModel.find().exec();
  }

  async countUser(request?: UserFilterRequest): Promise<number> {
    const filter = await this.filterUser(request);
    return this.userModel.countDocuments(filter);
  }

  async findOne(
    filter: FilterQuery<UserDocument>,
    projection?: any,
  ): Promise<User> {
    return await this.userModel
      .findOne(filter, projection)
      .populate('createdBy');
  }

  async findByUsernameOrEmail(username: string): Promise<User> {
    return await this.userModel
      .findOne({
        $or: [{ email: username }, { username }],
      })
      .populate('createdBy');
  }

  async findByEmail(email: string): Promise<User> {
    return await this.userModel
      .findOne({ email }, { password: false })
      .populate('createdBy');
  }

  async findById(id: string): Promise<User> {
    return await this.userModel.findById(id).populate('createdBy').exec();
  }

  async updateById(id: string, user: User): Promise<User> {
    return await this.userModel
      .findByIdAndUpdate(id, { $set: user }, { new: true })
      .populate('createdBy');
  }

  async deleteById(id: string) {
    return this.userModel.findByIdAndDelete(id).populate('createdBy');
  }
}
